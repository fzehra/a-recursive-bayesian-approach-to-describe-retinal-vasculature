%%DEMO 
DatasetName='HRIS';
addpath('Functions')

switch DatasetName
    case 'CLRIS'
        Dataset.UpSamplingFactor=1/0.35;
        
    case 'HRIS' %% images were downsampled by a factor of 4
        Dataset.UpSamplingFactor=1;
        
    case 'VDIS'
        Dataset.UpSamplingFactor=1/0.5;
        
    case 'KPIS1'
        Dataset.UpSamplingFactor=1;
        
    case 'KPIS2'
        Dataset.UpSamplingFactor=1;
        
end


Dataset.Name= sprintf('%s%s',DatasetName,'.txt');
Path.ReferenceData= 'ReferenceData';
Path.Results=fullfile('Results',DatasetName);
Path.ProbabilityMaps= fullfile('ProbabilityMaps',DatasetName);
Path.RawImages= fullfile('RawImages',DatasetName);


%%

fileID = fopen(fullfile(Path.ReferenceData,Dataset.Name),'r');
A = (textscan(fileID,'%d%d%d%f%f%f%f%f%f%f%f%f%f%f%f'));

ImageSegmentNoSet= [A{2}, A{3}]; % [ image no, segment no]
ImageSegmentNoUnique = unique(ImageSegmentNoSet,'rows');

%% load data

load(fullfile(Path.ProbabilityMaps,'Rcent'));  % centerline detection
load(fullfile(Path.ProbabilityMaps,'Rbound'));  % boundary detection
load(fullfile(Path.ProbabilityMaps,'Rves'));  % vessel segmentation
load(fullfile(Path.RawImages, 'Images'));
I=double(I);

%% Initialise parameters for tracking
%rng(0)
%%
FigureOn=0;

%%
PreviousImageNo=0;

for i=1:size(ImageSegmentNoUnique,1)
    
    ImageSegmentNo= ImageSegmentNoUnique(i,:)
    Seed=ReadReferenceData(Path,Dataset,ImageSegmentNo);
    EigenVectorField=[];
    
    %% load image to be tracked
    
    ImageNo=ImageSegmentNo(1);
    
    
    if (ImageNo-PreviousImageNo)>0 
        ObservationCell=CreateObservationCell(Rves(:,:,ImageNo),Rcent(:,:,ImageNo),Rbound(:,:,ImageNo),I(:,:,ImageNo),Dataset);
    end
    %% in order to autumatically stop tracking, generate a binary map showing the last location to be tracked for a given vessel segment.
    FinishingMap=zeros(size(I,1),size(I,2));
    FinishingMap(round(Seed.FinishLocation(2)),round(Seed.FinishLocation(1)))=1;
    SE = strel('octagon', 6);
    FinishingMap= imdilate(FinishingMap,SE);
    
    %% track vessel segments
    History= TrackVesselSegments(Seed, ObservationCell,EigenVectorField,FinishingMap,FigureOn);
    
    MatriceToTableSmall(History,Path,ImageSegmentNo);
    
    PreviousImageNo=ImageNo;
end
%% if one wants to observe estimated vessel profiles on edge probability maps, 
%%he or she should uncomment the following lines
% EdgeProbMaps=NaNReplacement(Rbound);
% EdgeProbMaps=imresize(EdgeProbMaps,Dataset.UpSamplingFactor);
% EdgeProbMaps=EdgeProbMaps(1:size(Im,1),1:size(Im,2),:);
% I= EdgeProbMaps;
%% in order to observe estimated vessel profiles on fundus images
[AccuracyPrecisonProfile,AccuracyPrecisonSegments,ProfileWiseDifs]=PerformanceEvaluation(Path.Results, I);

save(fullfile(Path.Results,'AccuracyPrecision.mat'),'AccuracyPrecisonProfile','AccuracyPrecisonSegments','ProfileWiseDifs')

