%% write matrice to table 
function MatriceToTable(History,Path,ImageSegmentNo)
Matrice= History.Graph.Matrice;
Matrice=single(Matrice(2:end,:));

TrackingStep= Matrice(:,1);
InteriorLocX=  Matrice(:,2);
InteriorLocY=  Matrice(:,3);
EdgeLocs_1X=  Matrice(:,4);
EdgeLocs_1Y=  Matrice(:,5);
EdgeLocs_2X=  Matrice(:,6);
EdgeLocs_2Y=  Matrice(:,7);
Width= Matrice(:,8);

T = table(TrackingStep,InteriorLocX,InteriorLocY, EdgeLocs_1X,EdgeLocs_1Y,EdgeLocs_2X,EdgeLocs_2Y,Width);
writetable(T,fullfile(Path.Results,'TextFiles',sprintf('EstimatedVesselSegment%d_%d.txt',ImageSegmentNo(1),ImageSegmentNo(2))),'Delimiter',' ')


