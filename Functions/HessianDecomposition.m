function EigenVectorField= HessianDecomposition(I)

[r,c]=size(I);
[Gx,Gy]=imgradientxy(I);
[Gxx, Gxy] = imgradientxy(Gx); 
[Gyx, Gyy] = imgradientxy (Gy);

EigenVectorField= zeros(r,c,6);
for  i=1:r
    for j=1:c
      A=[Gxx(i,j),Gxy(i,j);Gyx(i,j), Gyy(i,j)];        
       [V,D]= eig(A);
       eigenValues= diag(D);
       EigenVectorField(i,j,1:2)=eigenValues; 
       EigenVectorField(i,j,3:end)=reshape(V,1,4);
    end
end

  