
function PlotProfiles(ObservationCell,TracedPath,ParticleSet,L,cs,es)

NuofProfiles=3;
IndisSampledProfiles= randi(ParticleSet.Number,NuofProfiles,1); % select random profiles
SampledProfiles=IndisSampledProfiles(1:NuofProfiles);

%% find the location of the vessel part under tracking
ExpectPriorProb=sum((ParticleSet.State).*repmat(ParticleSet.Weight,1,size(ParticleSet.State,2)),1);
% arbitrary vesssel interior location (Ax,Ay)
Ax= floor(ExpectPriorProb(3));
Ay= floor(ExpectPriorProb(4));
%% ObservationCell{5} is the gray fundus image
UntracedImage= ObservationCell{5}.*(1-TracedPath);
WS=20; % the window size to illustrate tracking is [20,20]
Colours={[.9,.0,.0],[.0,.9,.0],[.0,.0,.9]}; %% colurs for sampled profiles 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(3)
%% plot estimated vessel profiles on fundus image of the interest
subplot(1,2,1) 
imagesc(UntracedImage(Ay-WS:Ay+WS,Ax-WS:Ax+WS)), colormap(gray)
set(gca,'xtick',[]),set(gca,'ytick',[])
title('estimated vessel profiles')
hold on
for i=1:length(IndisSampledProfiles)
    rr=IndisSampledProfiles(i); 
    hold on
    line([ParticleSet.State(rr,7)-(Ax-WS) ParticleSet.State(rr,9)-(Ax-WS)], [ParticleSet.State(rr,8)-(Ay-WS),ParticleSet.State(rr,10)-(Ay-WS)],'Color',Colours{i}, 'LineWidth',4)
    line([ParticleSet.State(rr,7)-(Ax-WS)], [ParticleSet.State(rr,8)-(Ay-WS)],'Color',Colours{i}, 'LineWidth',4) 
end

hold off

%% plot edge and centreline probability profiles for sampled profiles
subplot(1,2,2),
title('estimated edge and centreline probability profiles')
for i=1:length(IndisSampledProfiles)
    rr=IndisSampledProfiles(i);
    plot(1:101,cs(rr,:),'LineStyle','-','Color',Colours{i},'LineWidth', 4);
    hold on
    plot(1:101,es(rr,:),'LineStyle','-.','Color',Colours{i},'LineWidth', 4)
    ylim([0,1])
    xlim([1,101])
end
legend(num2str(L(SampledProfiles(1))),'',num2str(L(SampledProfiles(2))),'',num2str(L(SampledProfiles(3))),'');
hold off

