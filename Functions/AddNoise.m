function NoisyVariable= AddNoise(Variable,ParticleNo,Std,DistributionType)
if size(Variable,1)==1
    
    if strcmp(DistributionType,'rand')
        NoisyVariable= repmat(Variable,ParticleNo,1)+(-repmat(Std*Variable,ParticleNo,1)+repmat(Std*Variable,ParticleNo,1)).*rand(ParticleNo,size(Variable,2));
        
    else
        NoisyVariable= repmat(Variable,ParticleNo,1)+Std*randn(ParticleNo,size(Variable,2));
    end
    
else
    if strcmp(DistributionType,'rand')
        NoisyVariable= Variable-Std*Variable+Std*Variable.*rand(ParticleNo,size(Variable,2));      
    else
        NoisyVariable= Variable+Std*randn(ParticleNo,size(Variable,2));
    end
end
