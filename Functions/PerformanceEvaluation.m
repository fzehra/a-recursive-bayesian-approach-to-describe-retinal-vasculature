function [AccuracyPrecisonProfile,AccuracyPrecisonSegments,Difs]=PerformanceEvaluation(pathToRead,I)
pathToSave=fullfile(pathToRead,'Figures');
pathToRead=fullfile(pathToRead,'TextFiles');
D= dir(fullfile(pathToRead,'*.txt')); % 78 + -> reference

FileNames= {D.name};
nuOfIm= numel(FileNames)/2;
PreviousImage_No=0;
Difs=[];
AccuracyPrecisonSegments=[];

for n=1:nuOfIm
    
    ReferenceFiles=FileNames{n+nuOfIm};
    EstimatedFiles=FileNames{n};
    
    
    
    Image_Segment_No=sscanf(ReferenceFiles, ['ReferenceVesselSegment','%d','_','%d']);
    Image_No=Image_Segment_No(1);
    Segment_No=Image_Segment_No(2);
    
  
  
    if PreviousImage_No~=Image_No
        if n>1
        savefig(h1,fullfile(pathToSave,num2str(PreviousImage_No)));
       
        end
        h1= figure;
        imagesc((I(:,:,Image_No))), colormap(gray), set(gca,'xtick',[]),set(gca,'ytick',[])
        PreviousImage_No=Image_No;
    end
    
    %% read reference vessel parameters
    fileID = fopen(fullfile(pathToRead,ReferenceFiles),'r'); % reference
    C = textscan(fileID,'%s%s%s%s%s%s%s',1); % get captions
    A = (textscan(fileID,'%f%f%f%f%f%f%f')); % get values
    fclose(fileID);
    
   %% read estimated vessel parameters
    fileID = fopen(fullfile(pathToRead,EstimatedFiles),'r'); % predicted
    C = textscan(fileID,'%s%s%s%s%s%s%s%s',1);
    B = (textscan(fileID,'%f%f%f%f%f%f%f%f'));
    fclose(fileID);
   %% plot reference (red) and estimated (green) profiles on fundus images

    for i=1:3:numel(A{1})
        hold on
        line([A{3}(i) A{5}(i)], [A{4}(i),A{6}(i)],'Color',[.9 .0 .0],'LineWidth',4) % reference
    end
    
    for i=1:3:numel(B{1})
        hold on
        line([B{4}(i) B{6}(i)], [B{5}(i),B{7}(i)],'Color',[.0 .9 .0],'LineWidth',4) % estimated
    end
    
%% interpolate equal number of points from estimated and reference widths.      
[DifferenceW,yyRef,yyEst]= CalculateOneToOneWidth(A{7},B{8});

Difs=cat(1,Difs,DifferenceW);
    
AccuracySegment=mean(abs(DifferenceW));
PrecisonSegment=std(DifferenceW);
%% Performance Criteria Over Vessel Segments
AccuracyPrecisonSegments=cat(1,AccuracyPrecisonSegments,[AccuracySegment,PrecisonSegment]);

  
end

savefig(h1,fullfile(pathToSave,num2str(Image_No)));

%% Performance Criteria Over Vessel Profiles
AccuracyPrecisonProfile=[mean(abs(Difs)),std(Difs)];









