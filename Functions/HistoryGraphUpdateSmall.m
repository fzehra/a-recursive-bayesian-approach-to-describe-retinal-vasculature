function History= HistoryGraphUpdate(History, TrackBifurcation,TrackNewSeed,SeedLocToBeAdded,TrackSameSeed)
History.Graph.TrackBifurcation=TrackBifurcation;
History.Graph.TrackNewSeed=TrackNewSeed;
History.Graph.SeedLocToBeAdded=SeedLocToBeAdded;
History.Graph.TrackSameSeed=TrackSameSeed;

