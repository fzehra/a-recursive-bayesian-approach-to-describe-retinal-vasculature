function CenterLocProb= CenterlineProbPerPixel(ObservationCell, loc)
 [X,Y] = meshgrid(1:size(ObservationCell{1},2),1:size(ObservationCell{1},1));
 CenterLocProb=interp2(X,Y,ObservationCell{1},loc(1),loc(2));
