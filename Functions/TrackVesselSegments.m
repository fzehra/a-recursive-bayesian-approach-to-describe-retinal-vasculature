function [History]= TrackVesselSegments(Seed, ObservationCell,EigenVectorField,FinishingMap,FigureOn)

ParticleSet= InitializeParticleSet(Seed);
InitializeHistory;
History= InitialiseHistoryGraph(History,[Seed.Initial.Loc,Seed.Initial.Width]);
%%
StopFlag= 0;
for i=1:400 %% the maximum tracking step number for tracing a vessel segment
    [ParticleSet,StopFlag]= ParticleFiltering(ParticleSet, ObservationCell,History.TracedPath,FigureOn,i);  %% centerline location and direction
    StopFlag=0;
    if StopFlag
        fprintf('The tracking stopped when tracing a regular vessel 1 \n');
        break
    else
        
        
        if i==1
            History= HistoryUpdateSmall (History,ParticleSet,[Seed.Initial.Loc,Seed.Initial.Width,Seed.Initial.Direction], ObservationCell{5},1);
            History= HistoryGraphUpdateSmall(History, 0,1,Seed.Initial.Loc, 0);
            History= ConnectivityGraphSmall(History,Seed.Initial.Loc);
            
        else
            History= HistoryUpdateSmall (History,ParticleSet,[], ObservationCell{5},0);
            History= HistoryGraphUpdateSmall(History,0,0,[],1);
            History= ConnectivityGraphSmall(History,[]);
        end
        
        
        
        if FigureOn
            PlotTracedPathOnRealImage(ObservationCell{5},ParticleSet,History);
            hold on
            plot(Seed.FinishLocation(1),Seed.FinishLocation(1),'*g')
        end
        
    end
    
    if and(i>1, FinishingMap(round(History.CenterLoc(end,2)),round(History.CenterLoc(end,1)))==1 )
        
        %% calculate the direction according to the final point in the vessel segment
        Distance=sum((repmat(Seed.FinishLocation,size(History.CenterLoc(:,:),1),1)- History.CenterLoc(:,:)).^2,2);
        [minV,minI] = min(Distance);
        if minI~=i
            break;
        end
    end
    
    
    
    
    
end

