%% 12.11.2017 i changed the place of expectation calculation. It was before resampling. It may change results. be careful

function [ParticleSet,StopFlag]= ParticleFiltering(ParticleSet,ObservationCell,TracedPath,FigureOn,iteration)

StopFlag=0;
%% geometric vessel model
ParticleSet= GeometricModel(ParticleSet,iteration); 

IndisSampledParticles = randsample(1:(ParticleSet.Number),(ParticleSet.Number),true,ParticleSet.Weight); %% sample from this prior probability density distribution 
ParticleSet.State=ParticleSet.State(IndisSampledParticles,:); 

%% update step %% calculate weights for resampling, which are the likelihoods of proposed particle locations
ParticleSet.Weight= ObservationModel(ParticleSet, ObservationCell,TracedPath,FigureOn); %% return likelihood for centerline

if sum(ParticleSet.Weight)==0 
    fprintf('All Particle Weights are zero \n');
    StopFlag=1;
    ParticleSet=[];
    
else
    
%% normalise weights
ParticleSet.Weight=(ParticleSet.Weight)/sum(ParticleSet.Weight(:));

%% resampling from the posterior probability distribution
IndisSampledParticles = randsample(1:(ParticleSet.Number),(ParticleSet.Number),true,ParticleSet.Weight);  %% sample from this posterior probability density distribution 
ParticleSet.State=ParticleSet.State(IndisSampledParticles,:); 
ParticleSet.Weight=ParticleSet.Weight(IndisSampledParticles,:);
%% assign equal weights to all particles after resampling
ParticleSet.Weight=ones((ParticleSet.Number),1)/(ParticleSet.Number);
%% calculate the expectation of the posterior probability distribution
ParticleSet.States.Expectation=sum((ParticleSet.State).*repmat(ParticleSet.Weight,1,size(ParticleSet.State,2)),1);

%% Stop Flag is not active during the vessel sgements
StopFlag=0;

if FigureOn
  figure(2)
  PlotHypothesisedEdgeLoc(ParticleSet,ObservationCell,TracedPath);
end

    
end 


