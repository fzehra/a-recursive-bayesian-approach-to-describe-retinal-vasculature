function PlotHypothesisedEdgeLoc(ParticleSet,ObservationCell,TracedPath)

a= floor(ParticleSet.States.Expectation(3));
b= floor(ParticleSet.States.Expectation(4));
Imm= ObservationCell{1}.*(1-TracedPath);
Edge=10;
imagesc(Imm(b-Edge:b+Edge,a-Edge:a+Edge)), colormap(gray)

hold on
plot(ParticleSet.State(:,3)-repmat((a-Edge),size(ParticleSet.State,1),1),ParticleSet.State(:,4)-repmat((b-Edge),size(ParticleSet.State,1),1),'*r',...
    ParticleSet.State(:,7)-repmat((a-Edge),size(ParticleSet.State,1),1),ParticleSet.State(:,8)-repmat((b-Edge),size(ParticleSet.State,1),1),'*g',...
    ParticleSet.State(:,9)-repmat((a-Edge),size(ParticleSet.State,1),1),ParticleSet.State(:,Edge)-repmat((b-Edge),size(ParticleSet.State,1),1),'*g');
hold on
plot(ParticleSet.States.Expectation(:,3)-repmat((a-Edge),size(ParticleSet.States.Expectation,1),1),ParticleSet.States.Expectation(:,4)-repmat((b-Edge),size(ParticleSet.States.Expectation,1),1),'Or',...
    ParticleSet.States.Expectation(:,7)-repmat((a-Edge),size(ParticleSet.States.Expectation,1),1),ParticleSet.States.Expectation(:,8)-repmat((b-Edge),size(ParticleSet.States.Expectation,1),1),'Og',...
    ParticleSet.States.Expectation(:,9)-repmat((a-Edge),size(ParticleSet.States.Expectation,1),1),ParticleSet.States.Expectation(:,Edge)-repmat((b-Edge),size(ParticleSet.States.Expectation,1),1),'Og');
hold on
quiver(ParticleSet.State(:,3)-repmat((a-Edge),size(ParticleSet.State,1),1),ParticleSet.State(:,4)-repmat((b-Edge),size(ParticleSet.State,1),1),ParticleSet.State(:,1),ParticleSet.State(:,2));
