%function CalculateVesselWidth

D= dir('*.txt'); % 78 + -> reference

FileNames= {D.name};
nuOfIm= numel(FileNames)/2;

for n=1%:nuOfIm
    ReferenceFiles=FileNames{n+nuOfIm};
    EstimatedFiles=FileNames{n};

    
     %% read estimated vessel parameters
    fileID = fopen(EstimatedFiles,'r'); % predicted
%     C = textscan(fileID,'%s%s%s%s%s%s%s%s',1);
%     B = (textscan(fileID,'%f%f%f%f%f%f%f%f'));
    
     C = textscan(fileID,'%s%s%s%s%s%s%s%s%s%s%s',1);
    B = (textscan(fileID,'%f%f%f%f%f%f%f%f%f%f%f'));
    fclose(fileID);
    
  % Edges= [B{4} B{6} B{5} B{7}];
   WidthFromParticleFiltering= B{8};
   WidthFromEdges= sqrt((B{4}-B{6}).^2+(B{5}-B{7}).^2);
   WidthsToCompare=[WidthFromParticleFiltering,WidthFromEdges];
   %figure, plot(1:size(WidthsToCompareDemo,1),WidthsToCompareDemo(:,1),'r',1:size(WidthsToCompareDemo,1),WidthsToCompareDemo(:,2),'g',...
   %1:size(WidthsToCompare,1),WidthsToCompare(:,1),'b',1:size(WidthsToCompare,1),WidthsToCompare(:,2),'m')
end