function [History]= ConnectivityGraph(History,SeedLoc)
    if History.Graph.TrackSameSeed
       History.Graph.PreviousNode = History.Graph.CurrentNodeNo;
    elseif History.Graph.TrackNewSeed
        History.Graph.BranchNo=History.Graph.BranchNo+1;
         if size(History.Graph.SeedLoc,1)==1
             History.Graph.PreviousNode= History.Graph.SeedLoc(1,3);
             
         end
    end
    
    History.Graph.CurrentNodeNo=History.Graph.CurrentNodeNo+1;
    History.Graph.Matrice=cat(1,History.Graph.Matrice,[History.Graph.CurrentNodeNo,History.CenterLoc(end,:),...
    History.EdgeLoc(end,:),History.Width(end)]);

