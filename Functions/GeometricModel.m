function ParticleSet= GeometricModel(ParticleSet,iteration)
%% add noise to direction vectors
ParticleSet.State(:,1:2)= AddNoise(ParticleSet.State(:,1:2),ParticleSet.Number,ParticleSet.SystemNoiseStd.Direction,''); % direction
VectorLength= sqrt(ParticleSet.State(:,1).^2 +ParticleSet.State(:,2).^2 );  
ParticleSet.State(:,1:2)= ParticleSet.State(:,1:2)./ repmat(VectorLength,1,2);


%% add noise to vessel interior locations
if iteration>3
ParticleSet.State(:,3:4)= AddNoise(ParticleSet.State(:,3:4),ParticleSet.Number,ParticleSet.SystemNoiseStd.Loc,''); % centerline    
end

%% calculate new vessel interior locations by using new direction vectors, previous interior locations and the step size
ParticleSet.State(:,3:4)= UpdateInteriorLoc(ParticleSet.State(:,3:4),ParticleSet.State(:,1:2),ParticleSet.StepSize);

%% calculate edge locations and widths given vessel interior locations
ParticleSet.State(:,5)= abs(AddNoise(ParticleSet.State(:,5),ParticleSet.Number,ParticleSet.SystemNoiseStd.Width,'')); %% width1  
ParticleSet.State(:,6)= abs(AddNoise(ParticleSet.State(:,6),ParticleSet.Number,ParticleSet.SystemNoiseStd.Width,'')); %% width2  
RightDirection= [ParticleSet.State(:,2), -ParticleSet.State(:,1)]; 
%% check if right direction turns to opposite side
EdgeLocs= FindEdges(ParticleSet.State(:,3:4),ParticleSet.State(:,5:6),RightDirection); 
ParticleSet.State(:,7:10)=EdgeLocs;


