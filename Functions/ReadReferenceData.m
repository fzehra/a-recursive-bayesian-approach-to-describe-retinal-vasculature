function Seed=ReadMarkedDataGeneral(Path,Dataset,ImageSegmentNo)

fileID = fopen(fullfile(Path.ReferenceData,Dataset.Name),'r');
A = (textscan(fileID,'%d%d%d%f%f%f%f%f%f%f%f%f%f%f%f'));
%% detect the first vessel segment in the first image
ImageNo=ImageSegmentNo(1);
SegmentNo=ImageSegmentNo(2);
SegmentPixels= find(and(A{3}==SegmentNo,A{2}==ImageNo));

%% reference data is the average of edge locations estimated by observer 1, 2 and 3
Rpx=(A{4}(SegmentPixels)+A{8}(SegmentPixels)+A{12}(SegmentPixels))/3;
Rpy=(A{5}(SegmentPixels)+A{9}(SegmentPixels)+A{13}(SegmentPixels))/3;
Lpx=(A{6}(SegmentPixels)+A{10}(SegmentPixels)+A{14}(SegmentPixels))/3;
Lpy=(A{7}(SegmentPixels)+A{11}(SegmentPixels)+A{15}(SegmentPixels))/3;


%% centerline Locations and vessel widths
CenterLocs= single([(Rpx+ Lpx)/2, (Rpy+ Lpy)/2]);
Widths= single(sqrt((Rpx-Lpx).^2+(Rpy-Lpy).^2));
Boundaries=single([Rpx,Rpy,Lpx,Lpy]);
T = table(CenterLocs,Boundaries,Widths);


writetable(T,fullfile(Path.Results,'TextFiles',sprintf('%s%d_%d.txt','ReferenceVesselSegment', ImageNo,SegmentNo)),'Delimiter',' ');

%% initialise seed locations and the direction of tracking from reference data 
Seed.Initial.Loc= CenterLocs(1,:);
Seed.Initial.Width= [Widths(1)/2,Widths(1)/2];
Seed.End.Loc= CenterLocs(end,:);
CenterDistant= CenterLocs(2,:)-CenterLocs(1,:);
Seed.Initial.Direction= (CenterDistant)/sqrt(sum(CenterDistant.^2));
Seed.FinishLocation= CenterLocs(end,:);


