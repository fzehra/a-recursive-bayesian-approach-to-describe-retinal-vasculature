
function PePc= ObservationModel(ParticleSet, ObservationCell,TracedPath,FigureOn)


n=101;   %% number of points to interpolate from probability profiles
PePc=[];
Xq=[];
Yq=[];
cs=[];
es=[];

%% subtract vessel parts previuosly traced from vessel centreline (Pc) and edge (Pe) probability maps
Pc= ObservationCell{2}.*(1-TracedPath); 
Pe= ObservationCell{3}.*(1-TracedPath);

[X,Y] = meshgrid(1:size(ObservationCell{1},2),1:size(ObservationCell{1},1));

%% return equally spaced 'n' locations (Xq,Yq) between hypothesised edge locations for each particle 
for i=1:ParticleSet.Number
    Xq=cat(2,Xq, linspace(ParticleSet.State(i,7), ParticleSet.State(i,9),n));
    Yq=cat(2,Yq,linspace(ParticleSet.State(i,8), ParticleSet.State(i,10),n));
end

%% the representations of centreline and edge probability profiles with 'n' locations. 
Pcq = interp2(X,Y,Pc,Xq,Yq,'bilinear'); 
Peq = interp2(X,Y,Pe,Xq,Yq,'bilinear');

for i=1:ParticleSet.Number
    c= Pcq((i-1)*n+1:i*n);
    cs=cat(1,cs,c);
    e= Peq((i-1)*n+1:i*n);
    es=cat(1,es,e);
end

%% the centreline probabilities at hypothesised arbitrary vessel interior
% locations; Pc(zA) in eqn (10)
PA=interp2(X,Y,Pc,ParticleSet.State(:,3),ParticleSet.State(:,4),'bilinear'); 

%% eigenvectors at hypothesised arbitrary vessel interior locations
for EiVec= 1:6
    EigenVectors(:,EiVec)= interp2(X,Y,ObservationCell{4}(:,:,EiVec),ParticleSet.State(:,3),ParticleSet.State(:,4),'bilinear');
end
%% calculate Ps in eqn (10): similarity of the eigenvectors to hypothesised vessel directions by particle filtering
Ps= CalculatePs(EigenVectors,[ParticleSet.State(:,1),ParticleSet.State(:,2)]);
%% Calculate Pc' and Pe' in eqn (10): (chi_1 = 25,chi_2 = 51 and chi_3 = 76;  E_l=1, E_r=101)
PePc=(1-cs(:,1)).*(1-cs(:,101)).*cs(:,51).*cs(:,25).*cs(:,76).*(1-es(:,51)).*(1-es(:,25)).*(1-es(:,76)).*es(:,1).*es(:,101);

PePc=PA(:,1).*PePc;
PePc=PePc.*Ps; 

if FigureOn
    PlotProfiles(ObservationCell,TracedPath,ParticleSet,PePc,cs,es)
end

