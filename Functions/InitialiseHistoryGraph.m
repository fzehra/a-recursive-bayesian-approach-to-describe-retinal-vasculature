function History= InitialiseHistoryGraph(History,SeedLocToBeAdded)

History.CenterLoc= [];
History.EdgeLoc=[];
History.Direction=[];
History.Width=[];

History.Graph=[];
History.Graph.PreviousNode=0;
History.Graph.CurrentNodeNo=0;
History.Graph.TrackSameSeed=1;
History.Graph.TrackNewSeed=0;
History.Graph.TrackBifurcation=0;
History.Graph.BranchNo=1;
History.Graph.Matrice=[];
History.Graph.SeedLoc=[];
History.Graph.SeedLocToBeAdded=SeedLocToBeAdded; 