
function EdgeLocs= FindEdges(CentLoc,Widths,RightDirection)
EdgeLoc1= CentLoc + repmat(Widths(:,1),1,2).*RightDirection;
EdgeLoc2= CentLoc-repmat(Widths(:,2),1,2).*RightDirection;
EdgeLocs= [EdgeLoc1,EdgeLoc2];