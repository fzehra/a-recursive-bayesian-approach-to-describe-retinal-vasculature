%% Mode 3 calculate edge locations from symmetric vessel boundaries
function History= HistoryUpdate (History,ParticleSet,Seeds, Im, TrackingMode)
switch TrackingMode
    
    case 0  %% add estimated vessel parameters during tracking to history structure.
        History.CenterLoc= cat(1,  History.CenterLoc,ParticleSet.States.Expectation(3:4));
        History.EdgeLoc= cat(1,History.EdgeLoc, ParticleSet.States.Expectation(7:10));
        History.Direction= cat(1,History.Direction,ParticleSet.States.Expectation(1:2));
        History.Width=cat(1,History.Width,sqrt((ParticleSet.States.Expectation(7)-ParticleSet.States.Expectation(9)).^2+...
            (ParticleSet.States.Expectation(8)-ParticleSet.States.Expectation(10)).^2));
        History= GenerateTracedPath(History,Im,[]);
        
        
    case 1  % add seed locations to history
        
        RightDirection= [Seeds(end),-Seeds(end-1)];
        EdgeLocs= FindEdges(Seeds(1:2),Seeds(3:4),RightDirection);
        History.EdgeLoc=cat(1,History.EdgeLoc,EdgeLocs);
        History.CenterLoc= cat(1,History.CenterLoc,Seeds(1:2));
        History.Direction=cat(1,History.Direction,Seeds(end-1:end));
        History.Width=cat(1,History.Width,sum(Seeds(3:4),2)); 
        
end
