function Location= UpdateInteriorLoc (Location,Direction,StepSize)
if and (size(Location,1)==1, size(Direction,1)>1)
Location= repmat(Location,size(Direction,1),1);
end
if size(StepSize,1)==1
 Location= Location+StepSize*Direction;
else
Location= Location+repmat(abs(StepSize),1,2).*Direction;    
end


