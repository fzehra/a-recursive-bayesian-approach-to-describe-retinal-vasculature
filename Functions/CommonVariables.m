
%Particles1.BifurcationThreshold=0.3;
StepSize=2;
ParticleSet.Number=700;
ParticleSet.WidthThreshold=0.5; 
ParticleSet.CenterProbThreshold=0.2; 
ParticleSet.ModeToDetermineCurrentState= 3; % 0: median, 1: mean, 2: maximum prob, 3: weighted mean