function PlotTracedPathOnRealImage(Im,ParticleSet,History)
figure(1)
Im=double(Im);
Im_TracedPath= double(zeros(size(Im,1),size(Im,2),3));
Im_TracedPath(:,:,1)= (Im-min(Im(:)))/(max(Im(:)-min(Im(:))));
Im_TracedPath(:,:,2)= double(History.TracedPath);
figure(1), imagesc(Im_TracedPath)

hold on
if ~ isempty(History.CenterLoc)
plot (ParticleSet.States.Expectation(:,3),ParticleSet.States.Expectation(:,4),'Og')
end