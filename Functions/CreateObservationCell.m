function ObservationCell=CreateObservationCell(Rves,Rcent,Rbound,Im,Dataset)
        %% remove Nan in the probability maps
        ObservationCell{1}= NaNReplacement(Rves);
        ObservationCell{2}= NaNReplacement(Rcent);
        ObservationCell{3}= NaNReplacement(Rbound);
        
        %% upsample probability maps before tracking
        ObservationCell{1}= imresize(ObservationCell{1},Dataset.UpSamplingFactor);
        ObservationCell{2}= imresize(ObservationCell{2},Dataset.UpSamplingFactor);
        ObservationCell{3}= imresize(ObservationCell{3},Dataset.UpSamplingFactor);
        
      
        %% normalize the upsampled probability maps
        ObservationCell{1}=(ObservationCell{1}-min(ObservationCell{1}(:)))/(max(ObservationCell{1}(:))-min(ObservationCell{1}(:)));
        ObservationCell{2}=(ObservationCell{2}-min(ObservationCell{2}(:)))/(max(ObservationCell{2}(:))-min(ObservationCell{2}(:)));
        ObservationCell{3}=(ObservationCell{3}-min(ObservationCell{3}(:)))/(max(ObservationCell{3}(:))-min(ObservationCell{3}(:)));
        
        %% remove extra pixels due to upsampling
        ObservationCell{1}=ObservationCell{1}(1:size(Im,1),1:size(Im,2));
        ObservationCell{2}=ObservationCell{2}(1:size(Im,1),1:size(Im,2));
        ObservationCell{3}=ObservationCell{3}(1:size(Im,1),1:size(Im,2));
        
        
        
        ObservationCell{4}=HessianDecomposition(ObservationCell{2}); %EigenVectorField;
        ObservationCell{5}=Im;