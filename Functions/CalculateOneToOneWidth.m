function [Dif,yyRef,yyEst]=CalculateOneToOneWidth(Ref,Est)

yyRef = spline(1:numel(Ref),Ref,linspace(1,numel(Ref),100));
yyEst = spline(1:numel(Est),Est,linspace(1,numel(Est),100));
Dif=(-yyEst+yyRef)';


