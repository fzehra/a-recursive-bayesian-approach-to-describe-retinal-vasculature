function CosineSimilarity= CalculatePs(EigenVectorMat,HypoDirection)
EigenValues= EigenVectorMat(:,1:2);
[MaxEV, maxEI]=max(EigenValues,[],2);

EigenVectors= reshape(EigenVectorMat(:,3:end)',2,2,size(EigenVectorMat,1));
for i=1:size(EigenVectorMat,1)
    EigenVecNo=maxEI(i);
    Direction(i,:)=squeeze(EigenVectors(:,EigenVecNo,i))';
    
end
Direction=Direction./repmat(sqrt(sum(Direction.^2,2)),1,2);
CosineSimilarity= abs(sum(HypoDirection.*Direction,2));

