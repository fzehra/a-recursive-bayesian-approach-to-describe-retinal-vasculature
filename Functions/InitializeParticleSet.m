function ParticleSet= InitializeParticleSet(Seed)
SmallestWidth=1;
ParticleSet=SystemNoiseStd; %% std for noise
ParticleSet.States.Expectation=[];

%%
ParticleSet.DirectionSign=1;
ParticleSet.StepSize=2;
ParticleSet.Number=700;
ParticleSet.WidthThreshold=0.5; 
ParticleSet.CenterProbThreshold=0.2; 
ParticleSet.ModeToDetermineCurrentState= 3; % 0: median, 1: mean, 2: maximum prob, 3: weighted mean
ParticleSet.Weight=ones((ParticleSet.Number),1)/(ParticleSet.Number); 


%Direction->normal distribution
%InitalInteriorLocation->normal distribution
%Width1,Width2 -> normal distribution


ParticleSet.State=[AddNoise(ParticleSet.DirectionSign*Seed.Initial.Direction,ParticleSet.Number,ParticleSet.SystemNoiseStd.Direction,''),... % rand
    AddNoise(Seed.Initial.Loc,ParticleSet.Number,ParticleSet.SystemNoiseStd.Loc,''),...
    AddNoise(Seed.Initial.Width(1),ParticleSet.Number,ParticleSet.SystemNoiseStd.Width,''),...
    AddNoise(Seed.Initial.Width(2),ParticleSet.Number,ParticleSet.SystemNoiseStd.Width,''),...
    zeros(ParticleSet.Number,4)]; % edge locations

ParticleSet.State=double(ParticleSet.State);


