function History= GenerateTracedPath(History,Im,SeedLoc)
m = size(Im,1);
n=size(Im,2);

if ~isempty(SeedLoc)
    
% xi = [SeedLoc(1),History.EdgeLoc(end,1),History.EdgeLoc(end,3),SeedLoc(3),SeedLoc(1) ];
% yi = [SeedLoc(2),History.EdgeLoc(end,2),History.EdgeLoc(end,4),SeedLoc(4),SeedLoc(2)];
% BW= poly2mask(double(xi), double(yi),m,n);
% History.TracedPath=double(( History.TracedPath+BW)>0);   
    
 else

xi = [History.EdgeLoc(end-1,1),History.EdgeLoc(end,1),History.EdgeLoc(end,3),History.EdgeLoc(end-1,3),History.EdgeLoc(end-1,1) ];
yi = [History.EdgeLoc(end-1,2),History.EdgeLoc(end,2),History.EdgeLoc(end,4),History.EdgeLoc(end-1,4),History.EdgeLoc(end-1,2)];
BW= poly2mask(double(xi), double(yi),m,n);
History.TracedPath=double(( History.TracedPath+BW)>0);

end
