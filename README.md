This repository contains the code for 'A Recursive Bayesian Approach To Describe 
Retinal Vasculature Geometry' paper at https://arxiv.org/abs/1711.10521 . Code is written in Matlab, and tested on 
R2016b on a Mac.  The code has not been tested on many different installations, so
comments are welcome to Zehra Uslu.

In order to estimate vessel widths for vessel segments in the REVIEW dataset, 
firstly, specify a dataset (e.g. HRIS) listed in 'Demo.m', then run 'Demo.m'. 
Results will be saved to 'Results' folder.  
